import Element from "./Element";

function createElement(type, props, children) {
    return new Element(type, props, children);
}

// 设置属性
function setAttrs(node, prop, value) {
    switch (prop) {
        case 'value':
            // input 和 textarea 设置value值是直接点value，其他标签才需要设置attr
            if(node.tagName === 'INPUT' || node.tagName === 'TEXTAREA') {
                node.value = value;
            } else {
                node.setAttribute(prop, value);
            }
            break;
        case 'style':
            // 设置样式
            node.style.cssText = value;
            break;
        default:
            // 其他都按属性设置
            node.setAttribute(prop, value);
            break;
    }
}

// 渲染函数
function render(vDom) {
    const { type, props, children } = vDom,
          el = document.createElement(type);

    // 遍历属性为元素赋值
    for(var key in props) {
        // 就是为 el 元素 设置属性为 key  值为 props[key]
        setAttrs(el, key, props[key]);
    }

    // console.log(el);
    // 遍历children
    children.map(c => {
        // 先判断c是否是Element构造出来的，是则再次render一遍
        c = c instanceof Element
          ? render(c)
          :document.createTextNode(c); // c为文本节点
        // console.log(c);
        // 将c添加到el中
        el.appendChild(c);
    });

    return el;
}

// 渲染到页面
function renderDom(rDom, rootEl) {
    rootEl.appendChild(rDom);
}

export {
    createElement,
    render,
    setAttrs,
    renderDom
};