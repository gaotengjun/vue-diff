import { createElement, render, renderDom } from './virtualDom';
import domDiff from './domDiff';
import doPatch from './doPatch';

const vDom1 = createElement('ul', { class: 'list', style: 'width:300px;height:300px;background-color:orange;' }, [
                createElement('li', { class: 'item', 'data-index': '0' }, [
                    createElement('p', { class: 'text' }, ['第1个列表项'])
                ]),
                createElement('li', { class: 'item', 'data-index': 1 }, [
                    createElement('p', { class: 'text' }, [
                        createElement('span', { class: 'title' }, [])
                    ])
                ]),
                createElement('li', { class: 'item', 'data-index': 2 }, ['第3个列表项'])
            ]);

const vDom2 = createElement('ul', { class: 'list-wrap', style: 'width:300px;height:300px;background-color:orange;' }, [
                createElement('li', { class: 'item', 'data-index': '0' }, [
                    createElement('p', { class: 'title' }, ['特殊列表项'])
                ]),
                createElement('li', { class: 'item', 'data-index': 1 }, [
                    createElement('p', { class: 'text' }, [])
                ]),
                createElement('div', { class: 'item', 'data-index': 2 }, ['第3个列表项'])
            ]);

            // console.log(vDom);
// 虚拟dom获取真实dom
const rDom = render(vDom1);

// 真实dom渲染到页面
renderDom(rDom, document.getElementById('app'));


// 生成补丁包
var patches = domDiff(vDom1, vDom2);
// console.log(rDom);
// 打补丁, 往真实dom上打补丁
doPatch(rDom, patches)

console.log(patches);