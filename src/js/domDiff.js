import {
    ATTR,
    TEXT,
    REPLACE,
    REMOVE
} from './patchTypes';

let patches = {}, // 补丁包
    vnIndex = 0;

function domDiff(oldVDOM, newVDOM) {
    let index = 0;
    vNodeWalk(oldVDOM, newVDOM, index);
    return patches;
}

function vNodeWalk(oldNode, newNode, index) {
    let vnPatch = []; // 内部的补丁
     
    if(!newNode) {
        // 没有新节点，则被删除
        vnPatch.push({
            type: REMOVE,
            index
        });
    } else if(typeof oldNode === 'string' && typeof newNode === 'string') { // 文本节点
 
        if(oldNode !== newNode) { // 节点内容修改
            vnPatch.push({
                type: TEXT,
                text: newNode
            })
        }
    } else if(oldNode.type === newNode.type) { // 新旧节点的类型相同
        // 获取新旧节点属性的补丁
        const attrPatch = attrsWalk(oldNode.props, newNode.props); 
        // console.log(Object.keys(attrPatch));
        if(Object.keys(attrPatch).length > 0) {
            // attrPatch不为空时 添加补丁
            vnPatch.push({
                type: ATTR,
                attrs: attrPatch
            })
        }

        childrenWalk(oldNode.children, newNode.children);
    } else {
        vnPatch.push({
            type: REPLACE,
            newNode
        })
    }

    if(vnPatch.length > 0) {
        patches[index] = vnPatch;
    }
}

function attrsWalk(oldAttrs, newAttrs) {
    let attrPatch = {};
    // console.log(oldAttrs, newAttrs);
    for(let key in oldAttrs) {
        // 修改属性
        if(oldAttrs[key] !== newAttrs[key]) {
            // 属性值不同 打补丁
            attrPatch[key] = newAttrs[key];
        }
    }

    // 新增属性
    for(let key in newAttrs) {
        if(!oldAttrs.hasOwnProperty(key)) {
            // 老属性中没有 新属性 则为添加
            // 打补丁
            attrPatch[key] = newAttrs[key];
        }
    }

    return attrPatch;
}

// 遍历children
function childrenWalk(oldChildren, newChildren) {
    oldChildren.map((c, idx) => {
        // 递归调用
        vNodeWalk(c, newChildren[idx], ++ vnIndex)
    })
}

export default domDiff;