import {
    ATTR,
    TEXT,
    REPLACE,
    REMOVE
} from './patchTypes';

import { setAttrs, render } from './virtualDom';
import Element from './Element';

let finalPatches = {}, // 保存补丁包，方便使用 不用一直往下传递
    rnIndex = 0; // 真实节点索引

function doPatch(rDom, patch) {
    // 保存补丁包
    finalPatches = patch;
    rNodeWalk(rDom);
}

function rNodeWalk(rNode) {
    // console.log(rNode);
    const rnPatch = finalPatches[rnIndex++], // 对应索引补丁包
          childNodes = rNode.childNodes; // 类数组

    // console.log(rnPatch, childNodes);
    [...childNodes].map(c => {
        // 递归处理
        rNodeWalk(c);
    });

    
    // 如果存在补丁就进行处理
    if(rnPatch) {
        // console.log(rNode, rnPatch);
        patchAction(rNode, rnPatch);
    }
}

// 打补丁操作
function patchAction(rNode, rnPatch) {
    // 遍历补丁 是个数组
    rnPatch.map(p => {
        switch(p.type) {
            case ATTR:
                // 遍历属性
                for(var key in p.attrs) {
                    const value = p.attrs[key];
                    // 当value存在时，需要给当前真实节点添加属性
                    if(value) {
                        setAttrs(rNode, key, value);
                    } else {
                        // 没有value，删除掉这个属性
                        rNode.removeAttribute(key);
                    }
                }
                break;
            case TEXT:
                // 文本补丁直接修改
                rNode.textContent = p.text;  // innerText textContent 兼容性不一样 效果一样
                break;
            case REPLACE:
                console.log(rNode);
                // 替换过后的为虚拟节点
                // 先要判断是否是Element构造，是怎需要使用render转换为真实节点
                const newNode = (p.newNode instanceof Element )
                              ? render(p.newNode) 
                              : document.createTextNode(p.newNode); // 普通文本节点
                // 获取当前节点的父节点，父节点进行老节点替换为新节点
                rNode.parentNode.replaceChild(newNode, rNode); // 将rNode老节点替换为newNode新节点
                break;
            case REMOVE:
                // 获取当前节点的父节点，删除rNode子节点
                console.log(rNode);
                rNode.parentNode.removeChild(rNode);
                break;
            default:
                break;
        }
    })
}

export default doPatch;

// vNode = virtual Node
// vnPatch = virtual Node patch
// rNode = real Node
// rnPatch = real Node patch