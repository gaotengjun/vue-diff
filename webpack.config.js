const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: resolve(__dirname, 'src/js/index.js'),
    output: {
        path: resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    devtool: 'source-map',
    plugins: [
        new HtmlWebpackPlugin({
            template: resolve(__dirname, 'src/index.html'),
        })
    ],
    devServer: {
        open: true,
        contentBase: './', // 设置静态文件目录
    }

}